describe('Acessar a pagina inicial do buscador', () => {
    it('Ir para a url', () => {
        browser.url('https://duckduckgo.com/');
    });
});

describe('Realizar uma busca e verificar se a pagina carregou', () => {
    it('Preencher o input de busca', () => {
        $('input#search_form_input_homepage').setValue('webdriverio');
    });
    it('Realizar a busca', () => {
        $('input#search_button_homepage').click();
    });
    it('Clicar no primeiro link', () => {
        $$('a.result__a')[0].click();
    });
    it('Verificar se a pagina foi carregada', () => {
        $('h2.headerTitle').waitForDisplayed(5000);
        expect(browser.getTitle()).toContain('Next-gen');
    });
});